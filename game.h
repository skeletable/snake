#include <iostream>
#include <vector>
#include <ncurses.h>
#include <cstdlib>
#include <ctime>
#include "structs.h"
#include "snake.h"
#include "fruit.h"

#ifndef GAME_H
#define GAME_H

class game {
    private:
        int score, tickrate;
        char direction;
        bool death();
        void initGame();
        void drawWindow();
        void printScore();
        void spawnFruit();
        bool eatFruit();
        Snake snake;
        Fruit fruit;
    public:
        int height, width;
        game();
        ~game();
        void runGame();
};

#endif // GAME_H
