#include <vector>
#include "structs.h"

#ifndef SNAKE
#define SNAKE

class Snake {
    private:
        char segment_style, direction;
    public:
        bool eatsFruit;
        std::vector<xyPos> body;
        Snake();
        void moveSnake();
        void drawSnake();
};

#endif // SNAKE
