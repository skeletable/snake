#include <cstdlib>

#ifndef STRUCTS_H_
#define STRUCTS_H_

struct xyPos {
        int x, y;
        xyPos(int col, int row);
        xyPos();
};

#endif // STRUCTS_H_
