#include "game.h"
#include "snake.h"
#include "structs.h"
#include "fruit.h"
#include <unistd.h>

xyPos::xyPos(int col, int row) {
    x = col;
    y = row;
}

xyPos::xyPos() {
    x = 0;
    y = 0;
}

game::game() {
    score = 0;
    tickrate = 120000;
    srand(time(NULL));
    initGame();
    drawWindow();
    printScore();
    spawnFruit();
    eatFruit();

    refresh();
}

game::~game() {
    nodelay(stdscr, false);
    getch();
    endwin();
}

Fruit::Fruit() {
    fruitchar = '*';
    fruitpos.x = 0;
    fruitpos.y = 0;
    fruit_value = 10;
}

Snake::Snake() {
    segment_style = 'o';
    direction = 'l';
    eatsFruit = false;
    drawSnake();
    moveSnake();
}

void game::initGame() {
    initscr();
    nodelay(stdscr, true);
    keypad(stdscr, true);
    noecho();
    curs_set(0);
    getmaxyx(stdscr, height, width);
    return;
}

void game::drawWindow() {
    for(int i = 0; i < width; i++) {
        move(0, i);
        addch('X');
    }
    for(int i = 0; i < width; i++) {
        move(height - 2, i);
        addch('X');
    }
    for(int i = 0; i < height; i++) {
        move(i, 0);
        addch('X');
    }
        for(int i = 0; i < height-1; i++) {
        move(i, width - 1);
        addch('X');
    }
    return;
}

void Snake::drawSnake() {
    for(int i = 0; i < 5; i++) {
        body.push_back(xyPos(30+i, 10));
    }
    for(int i = 0; i < body.size(); i++) {
        move(body[i].y, body[i].x);
        addch(segment_style);
    }
    return;
}

void game::printScore() {
    move(height - 1, 0);
    printw("Score: %d", score);
    return;
}

void game::spawnFruit() {
    while(1) {
        int randx = rand() % width + 1;
        int randy = rand() % height + 1;

        if(randx >= width - 2 || randy >= height - 3) {
            continue;
        }

        fruit.fruitpos.x = randx;
        fruit.fruitpos.y = randy;
        break;
    }

    move(fruit.fruitpos.y, fruit.fruitpos.x);
    addch(fruit.fruitchar);
    refresh();
}

bool game::death() {
    if(snake.body[0].x == 0 || snake.body[0].x == width - 1 || snake.body[0].y == 0 || snake.body[0].y == height - 2) {
        return true;
    }
    for(int i = 2; i < snake.body.size(); i++) {
        if(snake.body[0].x == snake.body[i].x && snake.body[0].y == snake.body[i].y) {
            return true;
        }
    }
    return false;
}

void Snake::moveSnake() {
    int pressedKey = getch();
    switch(pressedKey) {
        case KEY_LEFT:
            if(direction != 'r') {
                direction = 'l';
            }
            break;
        case KEY_RIGHT:
            if(direction != 'l') {
                direction = 'r';
            }
            break;
        case KEY_UP:
            if(direction != 'd') {
                direction = 'u';
            }
            break;
        case KEY_DOWN:
            if(direction != 'u') {
                direction = 'd';
            }
            break;
    }
    if(!eatsFruit) {
        move(body[body.size() - 1].y, body[body.size() - 1].x);
        printw(" ");
        refresh();
        body.pop_back();
    }

    switch(direction) {
        case 'l':
            body.insert(body.begin(), xyPos(body[0].x - 1, body[0].y));
            break;
        case 'r':
            body.insert(body.begin(), xyPos(body[0].x + 1, body[0].y));
            break;
        case 'u':
            body.insert(body.begin(), xyPos(body[0].x, body[0].y - 1));
            break;
        case 'd':
            body.insert(body.begin(), xyPos(body[0].x, body[0].y + 1));
            break;
    }
    move(body[0].y, body[0].x);
    addch(segment_style);
    refresh();
    return;
}

bool game::eatFruit() {
    if(snake.body[0].x == fruit.fruitpos.x && snake.body[0].y == fruit.fruitpos.y) {
        spawnFruit();
        score += fruit.fruit_value;
        printScore();
        return snake.eatsFruit = true;
    }
    return snake.eatsFruit = false;
}

void game::runGame() {
    while(1) {
        if(death()) {
            break;
        }
        eatFruit();
        snake.moveSnake();
        usleep(tickrate);
    }
}
